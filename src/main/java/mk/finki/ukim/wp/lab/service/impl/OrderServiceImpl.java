package mk.finki.ukim.wp.lab.service.impl;

import mk.finki.ukim.wp.lab.model.Order;
import mk.finki.ukim.wp.lab.service.OrderService;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class OrderServiceImpl implements OrderService {

    @Override
    public Order placeOrder(String balloonColor, String clientName, String address) {
        Random r = new Random();
        Order order = new Order(balloonColor, clientName, address);
        order.setOrderId(r.nextLong());

        return order;
    }
}
