package mk.finki.ukim.wp.lab.repository;

import mk.finki.ukim.wp.lab.model.Balloon;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class BalloonRepository {


    public static List<Balloon> balloons = new ArrayList<>(10);


    @PostConstruct
    public void init() {
        /*balloons.add(new Balloon("Red", "Red balloon"));
        balloons.add(new Balloon("Red", "Red balloon"));
        balloons.add(new Balloon("Red", "Red balloon"));
        balloons.add(new Balloon("Red", "Red balloon"));
        balloons.add(new Balloon("Red", "Red balloon"));
        balloons.add(new Balloon("Red", "Red balloon"));
        balloons.add(new Balloon("Red", "Red balloon"));
        balloons.add(new Balloon("Red", "Red balloon"));
        balloons.add(new Balloon("Red", "Red balloon"));
        balloons.add(new Balloon("Red", "Red balloon"));*/
    }


    public List<Balloon> findAllBalloons() {
        return balloons;
    }

    public List<Balloon> findAllByNameOrDescription(String text) {
        return balloons.stream().filter(r->r.getName().contains(text) || r.getDescription().contains(text)).collect(Collectors.toList());
    }
}
