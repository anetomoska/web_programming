package mk.finki.ukim.wp.lab.web.servlets;

import mk.finki.ukim.wp.lab.model.Order;
import mk.finki.ukim.wp.lab.model.exception.InvalidArgumentsException;
import mk.finki.ukim.wp.lab.service.OrderService;
import org.thymeleaf.context.WebContext;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "orderBalloon", urlPatterns = "/BalloonOrder.do")
public class BalloonOrderServlet extends HttpServlet {

    private final SpringTemplateEngine springTemplateEngine;

    private final OrderService orderService;

    public BalloonOrderServlet(SpringTemplateEngine springTemplateEngine, OrderService orderService) {
        this.springTemplateEngine = springTemplateEngine;
        this.orderService = orderService;
    }


   /* public BalloonOrderServlet(SpringTemplateEngine springTemplateEngine) {
        this.springTemplateEngine = springTemplateEngine;
    }*/

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        WebContext context = new WebContext(req, resp, req.getServletContext());
        springTemplateEngine.process("deliveryInfo.html", context, resp.getWriter());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String clientName = req.getParameter("clientName");
        String clientAddress = req.getParameter("clientAddress");
        String balloonColor = req.getParameter("balloonColor");

        /*WebContext context = new WebContext(req, resp, req.getServletContext());
        HttpSession session = context.getSession();

        Order order = (Order) session.getAttribute("selectBalloon");
        context.setVariable("selectBalloon", order);

        String size = req.getParameter("size");
        context.setVariable("size", size);
        session.setAttribute("size", size);

         */

        Order order = null;
        try {
            order = orderService.placeOrder(balloonColor, clientName, clientAddress);
        } catch (InvalidArgumentsException ex) {
            WebContext context = new WebContext(req, resp, req.getServletContext());
            //context.setVariable();
            springTemplateEngine.process("listBalloons.html", context, resp.getWriter());
        }
        req.getSession().setAttribute("order", order);
        resp.sendRedirect("/ConfirmationInfo");
    }
}
