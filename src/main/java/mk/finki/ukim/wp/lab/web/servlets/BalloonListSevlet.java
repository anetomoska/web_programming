package mk.finki.ukim.wp.lab.web.servlets;

import mk.finki.ukim.wp.lab.model.Balloon;
import mk.finki.ukim.wp.lab.service.BalloonService;
import org.thymeleaf.context.WebContext;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "balloonServlet", urlPatterns = "/balloon")
public class BalloonListSevlet extends HttpServlet {

    //mora da se instancira springEngine
    private final SpringTemplateEngine springTemplateEngine;

    //kreirame zavisnost so BalloonService
    private final BalloonService balloonService;

    public BalloonListSevlet(SpringTemplateEngine springTemplateEngine, BalloonService balloonService) {
        this.springTemplateEngine = springTemplateEngine;
        this.balloonService = balloonService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        WebContext context = new WebContext(req, resp, req.getServletContext());
        springTemplateEngine.process("listBalloons.html", context, resp.getWriter());
        HttpSession session = req.getSession();
        List<Balloon> balloons = balloonService.listAll();
        //context.setVariable("color", );
        //context.setVariable("balloons", balloonService.listAll());
        //balloonService.process("listBalloons.html", context, resp.getWriter()); -> ova e gresno

        /*
        //pecatenje na cela lista
        PrintWriter printWriter = resp.getWriter();
        printWriter.println("<html>");
        printWriter.println("<head>");
        printWriter.println("</head>");
        printWriter.println("<body>");
        printWriter.println("<h3>Balloon List</h3>");
        printWriter.println("<ul>");
        balloonService.listAll().forEach(r-> printWriter.format("<li>%s (%s)</li>", r.getName(), r.getDescription()));
        printWriter.println("</ul>");
        printWriter.println("</body>");
        printWriter.println("</html>");

         */
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String color = req.getParameter("color");
        req.getSession().setAttribute("order", order);
        resp.sendRedirect("/selectBalloon");
    }
}
